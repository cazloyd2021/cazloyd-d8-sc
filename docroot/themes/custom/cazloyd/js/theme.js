(function($){

  var widthScreen = $(window).width();
  var heightScreen = $(window).height();

  // Apply background colours alternation through javascript
  var background_color_alternation = {
    forced_to_gray : [],
    forced_to_white : [
      '.field--item.map'
    ],
    colour_white : '#ffffff',
    colour_gray : '#f1f2f6',
    counter_alternated_bgs : 0,
    current_colour : null,
    current_tag_class : null,
    current_colour_is_white : null,
    force_bg : function (targets, bg, tag_class){
      var target = null, i = 0;
      for( i; i < targets.length; i++ ){
        target = $(targets[i]);
        target.css('background-color', bg);
        target.addClass('stated_bg');
        target.attr('data-bg',tag_class);
        // this.complement_if_child_bigger(target);
      }
    },
    apply_current_colour : function(target){
      this.counter_alternated_bgs = this.counter_alternated_bgs + 1;
      target.css('background-color', this.current_colour);
      target.attr('data-bg',this.current_tag_class);
      target.attr('data-bg',this.current_tag_class);
      target.attr('data-bgalt-i',this.counter_alternated_bgs);
      // this.complement_if_child_bigger(target);
      this.run_through_children(target);
    },
    complement_if_child_bigger : function(target){
      if( target.width < $('body').width()){
        var children = target.find('> *');
        for( var i = 0; i < children.length; i++ ){
          child = children.eq(i);
          child.addClass('tsted');
          if ( child.length > 0 ){
            if( child.width() > target.width() ){
              child.css('background-color', this.current_colour);
              child.attr('data-bg',this.current_tag_class);
              child.addClass('complementBg');
            }
          }
        }
      }
    },
    run_through_children : function(target){
      var results = target.find('.field--item').not(target.find('.field--item .field--item')),
        parent = null,
        children = null,
        child = null;

      // target.css('border',results.length+'px solid red');
      // console.log('in run_through_children// lenght is '+results.length);
      for(var j = 0; j < results.length; j++ ){
        parent = results.eq(j).parent();
        if( !parent.hasClass('parent-blocked') ){
          // target.css('border','1px solid yellow');
          children = parent.find('> .field--item,> .paragraph--view-mode--default');
          for (var i = 0; i < children.length; i++ ){
            child = children.eq(i);
            if(!child.hasClass('stated_bg')){
              if((child.width() >= $('.region.region-content').width()) && (child.height() > 150)){
                this.apply_current_colour(child);
                if( results.length > 1 ){
                  this.alternate_colour();
                }
              }
            } else {
              child_data_bg = child.attr('data-bg');
              if(child_data_bg){
                if(child_data_bg === 'white'){ this.make_colour_gray(); }
                else if(child_data_bg === 'gray'){ this.make_colour_white(); }
              }
            }
          }
          parent.addClass('parent-blocked');
        }
      }
      $('body .parent-blocked').removeClass('parent-blocked');
    },
    alternate_colour : function(){
      if( this.current_colour_is_white ) {
        this.make_colour_gray();
      } else {
        this.make_colour_white();
      }
    },
    make_colour_white : function(){
      this.current_colour = this.colour_white;
      this.current_colour_is_white = true;
      this.current_tag_class = 'white';
    },
    make_colour_gray : function(){
      this.current_colour = this.colour_gray;
      this.current_colour_is_white = false;
      this.current_tag_class = 'gray';
    },
    set_init_colour : function(){
      var body = $('body');
      if( body.hasClass('path-destinations') ||
        body.hasClass('path-magazine') ||
        body.hasClass('experience_detail_v2') ||
        body.hasClass('destination_detail_v2') ){ this.make_colour_gray(); }
      else { this.make_colour_white(); }
    },
    init : function (){

      this.set_init_colour();
      this.force_bg(this.forced_to_gray,this.colour_gray,'gray');
      this.force_bg(this.forced_to_white,this.colour_white,'white');

      this.run_through_children($('body'));
    }
  };
  background_color_alternation.init();

  //////////////////////////////////////////////////
  //          DOM READY                           //
  //////////////////////////////////////////////////

  $(document).ready(function() {

    var widthScreen = $(window).width();
    var heightScreen = $(window).height();

    background_color_alternation.init();

    // Initialise the SVG convert plugin
    // https://github.com/bymayo/svg-convert
    // https://stackoverflow.com/questions/9529300/can-i-change-the-fill-color-of-an-svg-path-with-css
    if($("img").hasClass('svg-convert')) {
      $('.svg-convert').svgConvert();
    }
    
    // (To avoid using the the Youtube API), mimic the PLAY event using a custom button
    $('#play-video').on('click', function(ev) {
      $('#video')[0].src += "?autoplay=1";
      ev.preventDefault();
    });

    // Fading in/out the play button when videos are clicked
    var video = '.paragraph--type--video .video';

    $( video ).click(function() {

      $( "#play-button" ).animate({
         opacity: 0,
      }, 1000, function() {
      // Animation complete.
         $(this).css('z-index', -1);
         $('#play-video').css('z-index', -1);
      });

    });

    // Toggle the Search
    $( '.search-header img' ).click(function() {

      $('.form-search').focus();
      $( this ).toggleClass('search-is-visible');
      $( '.search-reveal' ).slideToggle( 'fast', function() { });

    });

    $('.close-search-form').click(function(){
      $('.search-reveal').slideUp(100);
      $('.search-header img').removeClass('search-is-visible');
    });
    //set focus on input (Curious search)
    setTimeout(function() { jQuery('input[name="curious_search"]').focus(); }, 1000);

    // Move the Region Title onto the Banner Tiles
    // Pierrick: Tmp solution - go back in tpl and sort
    $('.region h2.with-panel span').detach().prependTo('article .image-tiles .image-tiles-inner .image-banner-wrapper .image-banner-wrapper-inner .banner.image-50');

    // Wrapping Images uploaded in Blog Posts so we can apply the full element width + various styling helpers
    var blogImage = $('.page-node-type-blog .field--name-body p img');
    $(blogImage).wrap('<span class="blog-image"></span');
    $('span.blog-image').parent().addClass('p-blog-image');
    $('.p-blog-image').prev().addClass('prev-image');
    $('.p-blog-image').next().addClass('next-image');
    // iframe in wysiwyg
    var blogVideo = $('.blog .field--name-body iframe');
    $(blogVideo).wrap('<div class="width-control"></div>');
    $('div.width-control').parent().addClass('video');

    // Add an unique ID for multiple carousel on the same Pages (they use the same paragraph) - e.g Small Group Travel
    $('.field--item div .carousel:not(#social-feed-carousel)').each(function(n) {
      $(this).attr('id', 'wheel' + n).children('.carousel-control').attr('href', '#wheel' + n);
    });
    // Hide Bootstrap Carousel controls if only one slide
    $('.carousel-inner').each(function() {
      if ($(this).children('div').length === 1) $(this).siblings('.carousel-control, .carousel-indicators').hide();
     });


    // Move the /magazine Header View into the Nav area so both sticks
    $('.top-magazine-wrapper').detach().insertAfter('.region-navigation');

    // Move the Remaining Seats badge inside .image-banner-wrapper-inner so it can display in the far right corner
    $('.seats-remaining').detach().prependTo('.image-banner-wrapper-inner');

    // Forcing sub menu level 2 to open on click and the parent item to click through
    var allSubSubMenu = $( '.block-we-megamenu .subul > li .we-mega-menu-submenu');
    var subSubMenu = $( '.block-we-megamenu .subul > .dropdown-menu > a');
    $(subSubMenu).attr('data-click-state', '1');

    $(subSubMenu).on('click',function(){

      if ( ($(this).attr('data-click-state') == 1) && ( widthScreen < 768 ) ) {

        $(this).attr('data-click-state', 0);
        $(allSubSubMenu).hide('slow');
        $(this).next().toggle('slow');

        return false;

      } else {

        $(this).attr('data-click-state', 1);

        return true;

      }

    });

    // Destination sub sub menus
    var subsubmenu    = $('li[data-id="f3e75f65-4c3b-4170-8f06-c57211f9bf5b"] .we-mega-menu-submenu .subul');
    $(subsubmenu).attr('id','destinations');

    // Be Inspired menu
    $('li[data-id="2accaa37-822c-435f-a44f-505f391d1c2e"] .we-mega-menu-submenu ul').attr('id','beInspired');
    $('li[data-id="2accaa37-822c-435f-a44f-505f391d1c2e"] .we-mega-menu-submenu ul > li:lt(4)').wrapAll('<div class="be-inspired-col1"></div>');
    $('li[data-id="2accaa37-822c-435f-a44f-505f391d1c2e"] .we-mega-menu-submenu ul > li:lt(4)').wrapAll('<div class="be-inspired-col2"></div>');
    $('li[data-id="2accaa37-822c-435f-a44f-505f391d1c2e"] .we-mega-menu-submenu ul > li:lt(5)').wrapAll('<div class="be-inspired-col3"></div>');

    // Arrow down
    var scrollToTrigger = "#arrow-down";

    $(scrollToTrigger).click(function() {

      $('html,body').animate({
        scrollTop: $('#down-target').offset().top
      });

    });

    // Remove inline background color for Share Icons to comply with the design
    $('.share-bottom-post .icons>*>span').attr('style', '');

    // Cherry Picked Views - Layout (works in conjuction with the checkSize function)
    var convertLayout = $(function(){

      var cherryPickedList     = '.paragraph--type--cherry-pick-extraordinary-dest';
      var $cherryPickedLists   = $('.field--item').find('.paragraph--type--cherry-pick-extraordinary-dest');

      var wrapperList          = ' .list-wrapper ul >';

      $( $cherryPickedLists ).each(function() {

        if ( widthScreen > 769 ) {

          $( cherryPickedList + wrapperList + ' li:lt(4)').wrapAll('<div class="top"></div>');
          $( cherryPickedList + wrapperList + ' li:lt(4)').wrapAll('<div class="bottom"></div>');

          $( cherryPickedList + wrapperList + ' .bottom > li:lt(3)').wrapAll(' <div class="bottom-left"></div> ');
          $( cherryPickedList + wrapperList + ' .bottom > li:lt(1)').wrapAll(' <div class="bottom-right"></div> ');

          $( cherryPickedList + wrapperList + ' .top > li:lt(1)').wrapAll(' <div class="top-left"></div> ');
          $( cherryPickedList + wrapperList + ' .top > li:lt(3)').wrapAll(' <div class="top-right"></div> ');

          // Non cherry-picked
          $( wrapperList + ' li:lt(4)').wrapAll('<div class="top"></div>');
          $( wrapperList + ' li:lt(4)').wrapAll('<div class="bottom"></div>');

          $( wrapperList + ' .bottom > li:lt(3)').wrapAll(' <div class="bottom-left"></div> ');
          $( wrapperList + ' .bottom > li:lt(1)').wrapAll(' <div class="bottom-right"></div> ');

          $( wrapperList + ' .top > li:lt(1)').wrapAll(' <div class="top-left"></div> ');
          $( wrapperList + ' .top > li:lt(3)').wrapAll(' <div class="top-right"></div> ');

        }

      });

    }); // convertLayout

    var removeParaContentWrapperIfEmpty = $(function(){

      $('.field--item').each(function() {
        if ( $(this).children().length == 0 ) {
          $(this).addClass('empty');
        }
      });

    }); // removeParaContentWrapperIfEmpty

    // run test on initial page load
    checkSize();

    // Apply the mobile crop for mobile
    heroArea();

    // Be inspired > * extra markup removal - Pierrick TMP
    var heroImg = $('.page-node-type-experience-detail-v2 .image-banner-wrapper .image-banner-wrapper-inner .image-banner-wrapper .image-banner-wrapper-inner .image-100');
    var heroImgImg = $('.page-node-type-experience-detail-v2 .image-banner-wrapper .image-banner-wrapper-inner .image-banner-wrapper .image-banner-wrapper-inner .image-100 img');

    if ( heroImg.parent().parent().parent().is( '.image-banner-wrapper-inner' ) ) {

      heroImg.unwrap().css('width','100vw');
      $('.page-node-type-experience-detail-v2 .arrow-down').css('width','100vw');

    }

    // END OF Mobile View helpers
    //////////////////////

    /////////////////////
    // Instagram Feed Slider

    // On mobile, 1 item per slide
    if ( widthScreen < 769 ) {
      $('.paragraph--type--social-feed .carousel-inner .view-item').each(function() {
        $(this).wrap("<div class='item'></div>");
      });
    }
    // Tablet & up, 3 items per slide
    else {
      var items = $('.paragraph--type--social-feed .carousel-inner .view-item');
      for( i = 0; i < items.length; i+=3) {
        items.slice(i, i+3).wrapAll("<div class='item'></div>");
      }
    }

    // Give the first item an active class
    $('.paragraph--type--social-feed .carousel-inner .item:first-of-type').addClass('active');

    // Add id for the slider
    $('.paragraph--type--social-feed .carousel').attr('id', 'social-feed-carousel');

    // Append arrow nav
    $('.paragraph--type--social-feed .carousel-inner').after('<a class="carousel-control left" href="#social-feed-carousel" data-slide="prev"></a><a class="carousel-control right" href="#social-feed-carousel" data-slide="next"></a>');

    // END OF Instagram Feed Slider
    //////////////////////

    /////////////////////
    // Footer Menu PDF Links - Open in a new tab
    var footerMenuItem = $('.region-footer ul.menu li a');
    footerMenuItem.each(function(){
      if ( $(this).attr('href').indexOf('.pdf') != -1){
        $(this).attr('target', '_blank');
      }
    });
    // END OF Footer Menu PDF Links
    //////////////////////

  }); // Document ready


  //////////////////////////////////////////////////
  //       ALL ASSETS HAVE LOADED FUNCTION        //
  //////////////////////////////////////////////////

  $(window).on('load', function(){

    var moveAssetsHomepage = $(function(){

      $('.path-frontpage .field--item.map').detach().insertAfter('.field--name-field-paragraph-content .field--item:nth-of-type(2)');


      $('.field--item.insta').detach().insertBefore('.field--name-field-paragraph-content > .field--item:last');

    }); // moveAssetsHomepage

    var moveAssetsCelebLanding = $(function(){

      $('.page-node-type-celebrations-landing .field--item.map').detach().insertAfter('.field--name-field-paragraph-content .field--item:nth-of-type(2)');

    }); // moveAssetsCelebLanding

    // Update the Img src for the Instagram collage based on breakpoint
    // Pierrick: to be revisited
    if ( widthScreen < 769 ) {

      var instaImg = '.instagram-collage-wrapper .collage img';
      var instaDesktopSrc = '/sites/default/files/instagram-collage.png';
      var instaMobSrc    =  '/sites/default/files/instagram-collage-mob.png';

      $(instaImg).attr('src', instaMobSrc);

    }

    else {

     $(instaImg).attr('src', instaDesktopSrc);

    }


    // Selected form items coming from preprocessed emquire form
    $('.form-flex-buttons label input:checked').parent().addClass('input-selected');

    // Dest & Exp Home View - get the height() of the wrapping div and apply it to the deeper thumbnail and thus fill up the space
    var heightForDeepThumb = $('.top-right').height();
    $('.dest-exp .top-left li .dest-exp-thumbs').css('height', heightForDeepThumb);
    $('.dest-exp .bottom-right li .dest-exp-thumbs').css('height', heightForDeepThumb);
    $('.dest-exp .top-left li .dest-exp-thumbs').css('height', heightForDeepThumb);
    $('.dest-exp .bottom-right li .dest-exp-thumbs').css('height', heightForDeepThumb);

    background_color_alternation.init();

  }); // Document loaded





  //////////////////////////////////////////////////
  //          RESIZE BROWSER FUNCTION             //
  //////////////////////////////////////////////////


 $(window).resize(function() {

    heroArea();

    var instaImg = '.instagram-collage-wrapper .collage img';
    var instaDesktopSrc = '/sites/default/files/instagram-collage.png';
    var instaMobSrc    =  '/sites/default/files/instagram-collage-mob.png';

    if ( widthScreen < 769 ) {
      $(instaImg).attr('src', instaMobSrc);
    }

    else if ( widthScreen > 769 ) {

      $(instaImg).attr('src', instaDesktopSrc);
    }

   checkSize();


   // Hiding the fixed positioned autocomplete panel if window is being resized
   $("#ui-id-1").css("display" , "none");

 }); // Resize function


  //////////////////////////////////////////////////
  //          ONSCROLL FUNCTIONS                  //
  //////////////////////////////////////////////////
  $(function(){

    $(window).scroll(function() {

      var headerEl = $('header.navbar');
      var headerHeight = $('header.navbar').height();
      var scroll = $(window).scrollTop();

      if (scroll >= headerHeight) {
        headerEl.addClass('shrink');
      }
      else {
        headerEl.removeClass('shrink');
      }

    });


  });

  //////////////////////////////////////////////////
  //          STANDALONE FUNCTIONS                //
  //////////////////////////////////////////////////


function checkSize(){
  // Coping for the split Dest & Exp View on mobile - being split up using the above
  var marker             = '.main-container';
  var destExpView        = '.dest-exp ul';
  var inlineStyles       = '-webkit-flex-basis: 100%; -ms-flex-preferred-size: 100%; flex-basis: 100%;';
  var width100           = 'width: 100%';

  if ($(marker).width() < 768) {
    $( destExpView + ' .top > *').addClass('flex-basis-100');
    $( destExpView + ' .bottom > *').addClass('flex-basis-100');
    $( destExpView + ' .top .top-left li').addClass('width-100');
    $( destExpView + ' .top .top-left li').addClass('width-100');

    /* Cancelling out the position fixed of the navbar if on Mobile or Tablet */
    $('header.navbar').css('position','relative');

  }
  if ($(marker).width() > 768) {
     $( destExpView + ' .top > *').removeClass('flex-basis-100');
     $( destExpView + ' .bottom > *').removeClass('flex-basis-100');
     $( destExpView + ' .top .top-left li').removeClass('width-100');
     $( destExpView + ' .top .top-left li').removeClass('width-100');
  }
}

function heroArea() {

  // PIERRICK: Whilst srcset gets implemented if ever

  var newWidth = $(window).width();
  var newHeight = $(window).height();

  // Clipping the Img and attribute its Src to the parent DIV + add styles to make it look Crop Center
  var MobHeroContainer     = $('.image-h1-wrapper .image-banner-wrapper.mobile');
  var MobCrop              = $('.image-h1-wrapper .image-banner-wrapper.mobile .image-banner-wrapper-inner img');
  var FindMobCrop          = $( MobHeroContainer ).find( MobCrop );

  // Slight different markup for the Blog node...
  var MobHeroContainerBlog = $('.image-banner-wrapper.mobile');
  var MobCropBlog          = $('.image-banner-wrapper.mobile .image-banner-wrapper-inner div img');
  var FindMobCropBlog      = $( MobHeroContainerBlog ).find( MobCropBlog );

  // Slight different markup for the Basic Page...
  var MobHeroContainerPage = $('.field--name-field-croppable-image-mobile');
  var MobCropPage          = $('.field--name-field-croppable-image-mobile img');
  var FindMobCropPage      = $( MobHeroContainerPage ).find( MobCropPage );

  var DesktopHeroContainer = $('.image-h1-wrapper .image-banner-wrapper.desktop');
  var DesktopHeroImg       = $('.image-h1-wrapper .image-banner-wrapper.desktop .image-banner-wrapper-inner div img');
  var DesktopHeroImgSrc    = $(DesktopHeroImg).attr('src');
  var DesktopHeroImgBkg    = 'url(' + DesktopHeroImgSrc + ')';

  var HeroImg              = '.image-h1-wrapper .image-banner-wrapper.desktop .image-banner-wrapper-inner div > img';

  var DesktopHeroContainerPage      = '.paragraph--type--banner-simple-image-title- .field--name-field-image .field--item';
  var DesktopHeroImgPage            = '.paragraph--type--banner-simple-image-title- .field--name-field-image .field--item img';
  var DesktopHeroImgPageSrc         = $(DesktopHeroImgPage).attr('src');
  var DesktopHeroImgPageBkg     = 'url(' + DesktopHeroImgPageSrc + ')';

  if ( $(FindMobCrop).length || $(FindMobCropBlog).length || (FindMobCropPage).length ) {

    if ( newWidth < 768 ) {
      $(DesktopHeroContainer).addClass('invisible clipped').removeClass('visible unclipped');
      $(DesktopHeroContainerPage).addClass('invisible clipped').removeClass('visible unclipped');
      $(MobHeroContainer).addClass('visible unclipped').removeClass('invisible clipped');
      $(MobHeroContainerBlog).addClass('visible unclipped').removeClass('invisible clipped');
      $(MobHeroContainerPage).addClass('visible unclipped').removeClass('invisible clipped');
    }
    else if ( newWidth > 768 ) {
      $(DesktopHeroContainer).addClass('visible unclipped').removeClass('invisible clipped');
      $(DesktopHeroContainerPage).addClass('visible unclipped').removeClass('invisible clipped');
      $(MobHeroContainer).addClass('invisible clipped').removeClass('visible unclipped');
      $(MobHeroContainerBlog).addClass('invisible clipped').removeClass('visible unclipped');
      $(MobHeroContainerPage).addClass('invisible clipped').removeClass('visible unclipped');
    }

  }

  else {

    if ( newWidth < 768 ) {
      $(MobHeroContainer).addClass('clipped');
      $(MobHeroContainerBlog).addClass('clipped');
      $(DesktopHeroImg).addClass('clipped').parent().addClass('home-hero-image');
      $(DesktopHeroImgPage).addClass('clipped').parent().addClass('home-hero-image-page');
      $('.home-hero-image').css('background-image' , DesktopHeroImgBkg);
      $('.home-hero-image-page').css('background-image' , DesktopHeroImgPageBkg);
    }
    else if ( newWidth > 768 ) {
      $(DesktopHeroContainer).addClass('visible unclipped').removeClass('invisible clipped home-hero-image').css('background','none');
      $(DesktopHeroContainerPage).addClass('visible unclipped').removeClass('invisible clipped home-hero-image-page').css('background','none');
      $(DesktopHeroImgPage).addClass('visible unclipped').removeClass('invisible clipped home-hero-image-page').css('background','none');

      $(MobHeroContainer).addClass('invisible clipped').removeClass('visible unclipped');
      $(MobHeroContainerBlog).addClass('invisible clipped').removeClass('visible unclipped');
    }

  }

}


//////////////////////////////////////////////////
//              ENQUIRE FORM JS                 //
//////////////////////////////////////////////////

/*
  Used to simply clear everything out of the hidden field
  that stores currently selected small groups. Note this does NOT
  set/unset the CSS indicators.
  Author: John Cogan
 */
function clearSelectedSmallGroupHiddenField(){
    unsetVisuallyAllSelectedSmallGroups();

    $("#webform-submission-enquire-add-form input[name=selected_small_groups]").val('');
    $("#webform-submission-enquire-add-form input[id=edit-hidden-field-selected-small-group]").val('');
}

// Add a class to body if on the enquiry form page
if ($('.webform-submission-enquire-form').length) {
  $('body').addClass('enquire-form-page');
}


/*
  Visually unsets all small groups
 */
function unsetVisuallyAllSelectedSmallGroups(){
    // Name of CSS class that will be put on the active item
    var activeClassName = 'small-group-selected-div';

    // elementSource.classList.remove(activeClassName);

    $('.small-group-selected-div').each(function(i, obj) {
        obj.classList.remove('small-group-selected-div');
    });

    $('.small-group-col-selected').each(function(i, obj) {
        obj.classList.remove('small-group-col-selected');
    });
}

var radioHolidayType = $('#edit-i-would-like-to .form-radio');
var allLabels = $('#edit-i-would-like-to > .form-item label, #edit-region-buttons > .form-item label, #edit-which-destination-central-america > .form-item label, #edit-which-destination-asia > .form-item label, #edit-which-destination-africa > .form-item label');

var radioBudgetRange = $('#edit-budget-per-person .form-radio');
var budgetRangeLabels = $('#edit-budget-per-person > .form-item label');

var radioRegion = $('#edit-region-buttons .form-radio');
var regionDestLabels = $('#edit-region-buttons > .form-item label, #edit-which-destination-central-america > .form-item label, #edit-which-destination-asia > .form-item label, #edit-which-destination-africa > .form-item label');

var checkboxDestination = $('.about-your-trip-block .form-checkbox');
var destinations = $('#edit-which-destination-central-america > .form-item label, #edit-which-destination-asia > .form-item label, #edit-which-destination-africa > .form-item label');

// I would like to radio buttons - holiday type
$(radioHolidayType).on('click', function() {
  $(allLabels).removeClass('input-selected');
  $(this).parent('label').addClass('input-selected');

  // Clear small groups if Tailor made or Celebration button clicked
  if($(this).attr('id') !== 'edit-i-would-like-to-small-group-travel'){
      clearSelectedSmallGroupHiddenField();
  }

});

// I would like to radio buttons - budget range
$(radioBudgetRange).on('click', function() {
  $(budgetRangeLabels).removeClass('input-selected');
  $(this).parent('label').addClass('input-selected');
});

// Region Buttons
$(radioRegion).on('click', function() {
  $(regionDestLabels).removeClass('input-selected');
  $(this).parent('label').addClass('input-selected');

  // Clear small groups is region button clicked
  clearSelectedSmallGroupHiddenField();
});

// Destination Checkboxes
$(checkboxDestination).on('click', function() {
  $(this).parent('label').toggleClass('input-selected');
});

// Small Group Cards
$('.small-group-col').on('click', function() {
  $(this).toggleClass('small-group-col-selected');
});

})(jQuery);
