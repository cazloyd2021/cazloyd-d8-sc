#!/usr/bin/env bash

declare -a JSPATHS=("sites/all/modules/jquery_update"
                    "sites/all/modules/commerce_popup_cart/js"
                    "sites/all/modules/custom_search/js"
                    "sites/all/modules/search_autocomplete/js"
                    "sites/all/modules/google_analytics"
                    "sites/all/themes/rhythm/js"
                    "misc"
                    )

for p in "${JSPATHS[@]}"; do
  for i in $(find "$p" -iname *.js ! -iname *.min.js -type f); do
    LINES=`wc -l "$i" | awk '{print $1}'`
    if [ "$LINES" -gt 30 ]; then
      uglifyjs --verbose --compress --output "$i" -- "$i"
    fi
  done
done
