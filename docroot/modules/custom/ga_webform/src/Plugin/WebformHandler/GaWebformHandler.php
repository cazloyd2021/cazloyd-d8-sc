<?php

namespace Drupal\ga_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\webformSubmissionInterface;
use Drupal\ga_webfrom\FormatCompositeFields;

/**
 * Create a new node entity from a webform submission.
 *
 * @WebformHandler(
 *   id = "Cazloyd form handler",
 *   label = @Translation("Cazloyd form handler"),
 *   category = @Translation("Cazloyd form handler"),
 *   description = @Translation("Extra processing on Cazloyd form submit"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */

class GaWebformHandler extends WebformHandlerBase {

  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // Check webform ids.
    if (($webform_submission->getWebform()->id() == 'enquire') ||
        ($webform_submission->getWebform()->id() == 'newsletter_signup') ||
        ($webform_submission->getWebform()->id() == 'newsletter_signup_curious') ||
        ($webform_submission->getWebform()->id() == 'register_your_interest')) {
      // Webform submission values.
      $webformValues = $webform_submission->getData();

      // API send
      $gaApi = \Drupal::service('ga_api.api');

      // Send to constant contact.
      $gaApi->sendConstantContact($webformValues);

      // Send to peak 15.
      $gaApi->sendPeak15($webformValues);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->validateEmail($form_state);
    if ($webform_submission->getWebform()->id() == 'enquire') {
      $this->validateSmallGroupTripCheckboxes($form_state);
    }
  }

  /**
   * Validate email using Constant Contact API.
   */
  private function validateEmail(FormStateInterface $form_state) {
    $email_address_value = !empty($form_state->getValue('email_address')) ? Html::escape($form_state->getValue('email_address')) : NULL;

    // Skip empty unique fields or arrays (aka #multiple).
    if (empty($email_address_value) || is_array($email_address_value)) {
      return;
    }
    else {
      // Default PHP validate of emails.
      if (!filter_var($email_address_value, FILTER_VALIDATE_EMAIL)) {
        $form_state->setErrorByName('email_address', $this->t('EMAIL ADDRESS Invalid email address. Valid e-mail can contain only latin letters, numbers, \'@\' and \'.'));
      }
    }
  }

  /**
   * Validate small_group_trip_checkboxes field to empty or not.
   */
  private function validateSmallGroupTripCheckboxes(FormStateInterface $form_state) {
    if ($form_state->getValue('i_would_like_to') == 'small-group-travel' && empty($form_state->getValue('small_group_trip_checkboxes'))) {
      $form_state->setErrorByName('small_group_trip_checkboxes', $this->t('ON WHICH SPECIALIST SMALL GROUP TRIP WOULD YOU LIKE TO TRAVEL? field is required.'));
    }
  }
}
