<?php
include 'S3.php';

$s3 = new S3('AKIAI4ZYNH7NVUSGPMLA', 'A7w34t3spH48Hpv7y366AN82a3VI8PJRU7NsOxXi');

$uri = $_SERVER['REQUEST_URI'];
$client = str_replace('/itinerary/', '', $uri);
list($clientFolder, $itinerary) = explode('-', $client);

$objectPath = $clientFolder . '/' . $itinerary . '.pdf';

$object = $s3->getObject('cazenove-loyd-itinerary', $objectPath);

if (!$object || !empty($object->error)) {
    
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: /");
    exit();
    
} else {
    
    header("Content-Type: {$object->headers['type']}");
    //header("Content-Disposition: attachment; filename=" . $itinerary . '.pdf');
    header('Pragma: public');
    echo $object->body;
    exit();
}