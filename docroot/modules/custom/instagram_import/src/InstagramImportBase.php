<?php

namespace Drupal\instagram_import;

use GuzzleHttp\Client;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class InstagramImportBase.
 */
class InstagramImportBase {
  
  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;
  
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  
  /**
   * Constructs a InstagramImportBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->entityManager = $entity_manager;
    $this->configFactory = $config_factory;
  }
  
  /**
   * Import posts.
   */
  public function import() {
    $config = $this->configFactory->get('instagram_import.settings');
    
    if ($config->get('import')) {
      
      $token = $config->get('token');
      $count = $config->get('count');
      
      $httpClient = new Client();
      
      //      $storage = $this->entityManager->getStorage('node');
      //
      //      $query = $storage->getAggregateQuery();
      //      $query->aggregate('field_instagram_id', 'MAX');
      //      $result = $query->execute();
      
      $min_id = NULL;
      //      if (isset($result[0]['field_instagram_id_max'])) {
      //        $min_id = $result[0]['field_instagram_id_max'];
      //      }

      $response = $httpClient->request('GET',
        'https://api.instagram.com/v1/users/self/media/recent/', [
          'query' => [
            'access_token' => $token,
            'count' => $count,
            'min_id' => $min_id,
          ],
        ]);
      
      $body = (string) $response->getBody();
      $posts = json_decode($body);
      
      if (is_object($posts) && isset($posts->data) && count($posts->data) > 0) {
        foreach ($posts->data as $post) {
          if (!$this->postExists($post->id)) {
            $this->createNode($post);
          }
        }
      }
      
    }
  }
  
  /**
   * Creating node.
   *
   * @param \stdClass $post
   *   Instagram post for import.
   */
  public function createNode(\stdClass $post) {
    $storage = $this->entityManager->getStorage('node');
    
    /** @var \Drupal\node\NodeInterface $node */
    $node = $storage->create([
      'type' => 'instagram',
      'field_instagram_id' => $post->id,
      'field_instagram_author' => [
        'uri' => 'https://www.instagram.com/' . $post->user->username . '/',
        'title' => $post->user->username,
      ],
      'title' => (strlen($post->caption->text) > 0 ? substr($post->caption->text,
        0, 100) : 'Instagram ' . $post->id),
      'field_instagram_caption' => (strlen($post->caption->text) > 0 ? $post->caption->text : ''),
      'field_instagram_post' => $post->link,
      'created' => $post->created_time,
      'uid' => '1',
      'status' => 1,
    ]);
    
    $path_info = pathinfo($post->images->standard_resolution->url);
    $data = file_get_contents($post->images->standard_resolution->url);
    $dir = 'public://instagram/';
    if ($data && file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
      $file = file_save_data($data, $dir . $path_info['basename'],
        FILE_EXISTS_RENAME);
      $node->set('field_instagram_local_image', $file);
    }
    
    $node->save();
  }
  
  /**
   * Run all tasks.
   */
  public function runAll() {
    $this->import();
    $this->cleanup();
  }
  
  /**
   * Check if id does not already exist
   *
   * @param string $instagram_id
   *  Instagram post id
   */
  public function postExists($instagram_id) {
    $storage = $this->entityManager->getStorage('node');
    $query = $storage->getQuery();
    $query->condition('field_instagram_id', $instagram_id, '=');
    $query->condition('type', 'instagram');
    $result = $query->execute();
    $nodes = $storage->loadMultiple($result);
    if (count($nodes) > 0) {
      return TRUE;
    }
    return FALSE;
  }
  
  
  /**
   * Delete old posts.
   */
  public function cleanup() {
    $config = $this->configFactory->get('instagram_import.settings');
    $expire = $config->get('expire');
    
    if ($expire) {
      $storage = $this->entityManager->getStorage('node');
      $query = $storage->getQuery();
      $query->condition('created', time() - $expire, '<');
      $query->condition('type', 'instagram');
      $result = $query->execute();
      $nodes = $storage->loadMultiple($result);
      
      foreach ($nodes as $node) {
        $node->delete();
      }
    }
  }
  
}
