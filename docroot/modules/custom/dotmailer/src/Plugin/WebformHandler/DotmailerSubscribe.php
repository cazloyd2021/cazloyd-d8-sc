<?php

namespace Drupal\dotmailer\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\Core\Url;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\webformSubmissionInterface;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "dotmailer_webform_submission",
 *   label = @Translation("Dotmailer simple webform handler"),
 *   category = @Translation("Form Handler"),
 *   description = @Translation("Allows users to be registered to address books"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class DotmailerSubscribe extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;

  public function defaultConfiguration() {
    return [
      'dotmailer_username' => NULL,
      'dotmailer_password' => NULL,
      'dotmailer_addressbook' => NULL,
      'double_opt_in' => NULL,
      'subscription_label' => $this->t('Subscribe'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->httpClient = \Drupal::httpClient();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $settings = [
      'dotmailer_username' => $this->configuration['dotmailer_username'],
      'dotmailer_password' => $this->configuration['dotmailer_password'],
      'double_opt_in' => $this->configuration['double_opt_in'],
    ];

    $form['dotmailer_addressbook']=[ '#type'=>'textfield',
                                  '#title' => $this->t('Addressbook ID'),
                                  '#default_value'=>$this->configuration['dotmailer_addressbook'],
    ];

    $form['subscription_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('used for the label of the checkbox'),
      '#default_value' => $this->configuration['subscription_label'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
     parent::submitConfigurationForm($form, $form_state);

     $this->configuration['dotmailer_addressbook'] = $form_state->getValue('dotmailer_addressbook');
   }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $values = $webform_submission->getData();

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $operation = ($update) ? 'update' : 'insert';
    $this->remotePost($operation, $webform_submission);
  }

  /**
   * Execute a remote post.
   *
   * @param string $operation
   *   The type of webform submission operation to be posted. Can be 'insert',
   *   'update', or 'delete'.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission to be posted.
   */
  protected function remotePost($operation, WebformSubmissionInterface $webform_submission) {
    $request_post_data = $this->getPostData($operation, $webform_submission);
    
    if(!empty($request_post_data['country'])){
      $term = \Drupal\taxonomy\Entity\Term::load($request_post_data['country']);
      if(is_object($term)){
        $country = $term->getname();
      }
    }else
      $country = null;

    $id = $this->getWebform()->getOriginalId();
    
    // Webform title.
    $form_title = $this->getWebform()->get('title');
    $page_url = Url::fromUserInput('/form/' . $id, ['absolute' => TRUE])->toString();

    $username = $this->configFactory->get('dotmailer.settings')->get('dotmailer_username');
    $password = $this->configFactory->get('dotmailer.settings')->get('dotmailer_password');
    
    $url = $this->configFactory->get('dotmailer.settings')->get('dotmailer_baseurl')."/v2/address-books/".$this->configuration['dotmailer_addressbook']."/contacts";
   
    try {
      if(!empty($request_post_data['email_address'])){
        $firstname = !empty($request_post_data['first_name']) ? $request_post_data['first_name']:null;
        $lastname = !empty($request_post_data['last_name']) ? $request_post_data['last_name']:null;
        $telephone = !empty($request_post_data['telephone_number']) ? $request_post_data['telephone_number']:null;
        $email = !empty($request_post_data['email_address']) ? $request_post_data['email_address']:null;
        //$tell_us = !empty($request_post_data['tell_us_more_about_the_trip_you_want_to_travel_on']) ? $request_post_data['tell_us_more_about_the_trip_you_want_to_travel_on']:null;
        $how = !empty($request_post_data['how_did_you_hear_about_us_']) ? $request_post_data['how_did_you_hear_about_us_']:null;
        $signup = !empty($request_post_data['newsletter_signup']) ? $request_post_data['newsletter_signup']:null;

        if(!empty($signup)){
          $content = ['email'=>$email,
                      'optInType'=>'Double',
                      'dataFields'=>[['key'=>'FIRSTNAME','value'=>$firstname,],
                                    ['key'=>'LASTNAME','value'=>$lastname],
                                    ['key'=>'COUNTRY','value'=>$country],
                                    ['key'=>'TELEPHONE','value'=>$telephone],
                                   // ['key'=>'CLIENTINTERESTS','value'=>$tell_us],
                                    ['key'=>'SOURCE','value'=>$how]]
                      ];
        
          $request = $this->httpClient->post($url,['auth'=>[$username,$password],'json'=>$content]);
          if(is_object($request)){
            $response = json_decode($request->getBody());
        
            if ($request->getStatusCode() == '204' || $request->getStatusCode()== '200') {
              $this->loggerFactory->get('dotmailer')->notice('Webform "%form" results succesfully submitted to dotmailer. Response: @msg', [
                '@msg' => $email,
                '%form' => $form_title,
              ]
              );
            }else {
              $this->loggerFactory->get('dotmailer')->notice('dotmailer error when submitting Webform "%form": <pre>@error</pre>', [
                '@error' => print_r($response,1),
                '%form' => $form_title,
              ]
              );
            }
          }else{
            $this->loggerFactory->get('dotmailer')->notice('dotmailer error when submitting Webform "%form": <pre>@error</pre>', [
              '@error' => print_r($response,1),
              '%form' => $form_title,
            ]
            );
          }
        }
      }     
    }
    catch (RequestException $e) {
      watchdog_exception('dotmailer', $e);
    }
  }

  /**
   * Get a webform submission's post data.
   *
   * @param string $operation
   *   The type of webform submission operation to be posted. Can be 'insert',
   *   'update', or 'delete'.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission to be posted.
   *
   * @return array
   *   A webform submission converted to an associative array.
   */
  protected function getPostData($operation, WebformSubmissionInterface $webform_submission) {
    // Get submission and elements data.
    $data = $webform_submission->toArray(TRUE);

    // Flatten data.
    // Prioritizing elements before the submissions fields.
    $data = $data['data'] + $data;
    unset($data['data']);

    return $data;
  }
}
