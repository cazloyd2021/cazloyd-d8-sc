#!/usr/bin/env bash

# config

PROJECT="Cazloyd"
THISAUTOPUB="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"
GITURL="git@bitbucket.org:graphicalliance/cazloyd-d8.git"
DOCROOT="httpdocs"
MASTER="release-master"

# get publish profile name from ENVIRONMENT file

PROFILE=`cat ENVIRONMENT`

# if profile not found, exit

if [ "$PROFILE" == "" ]; then
  echo "No profile specified, aborting"
  mail -s "$PROJECT Autodeploy" hans@graphicalliance.co.uk <<< "No profile found"
  exit
fi

# if profile not valid exit [live, preprod, staging]

if [ "$PROFILE" != "live" -a "$PROFILE" != "admin" -a "$PROFILE" != "preprod" -a "$PROFILE" != "staging" ]; then
  echo "Profile not valid, aborting"
  mail -s "$PROJECT Autodeploy" hans@graphicalliance.co.uk <<< "Profile not valid"
  exit
fi

if [ "$PROFILE" == "live" ]; then

  HOMEDIR="/var/www/vhosts/www.cazloyd.com"
  MEDIADIR="/var/www/vhosts/www.cazloyd.com"

fi

if [ "$PROFILE" == "staging" ]; then

  HOMEDIR="/var/www/vhosts/staging.cazloyd.com"
  MEDIADIR="/var/www/vhosts/staging.cazloyd.com"

fi

# get latest tag (by revision number) from the master release

if [ -d ${MASTER} ]; then

  git -C ${MASTER} pull

else

  git clone ${GITURL} ${MASTER}

fi

TAGNAME=`/usr/bin/php get_latest_tag.php ${PROFILE} ${MASTER}`

if [ "$TAGNAME" == "0" ]; then
  echo "No tag found, aborting"
  mail -s "$PROJECT Autodeploy" hans@graphicalliance.co.uk <<< "No tag found for ${PROFILE}"
  exit
fi

# check if tag already published

CURRENTRELEASE=`ls -l ${DOCROOT} | awk '{print $11}'`
if [ "$CURRENTRELEASE" == "$TAGNAME/docroot" ]; then
  echo "Latest tag already published, aborting"
  exit
fi

# if the new tag folder already exists, delete it first to avoid clone error

if [ -d ${TAGNAME} ]; then

  rm -rf ${TAGNAME}

fi

# publish tag

git clone ${GITURL} ${TAGNAME}

if [ -d ${TAGNAME} ]; then

  cd ${TAGNAME}
  git checkout tags/${TAGNAME}

  # link shared folders

  rm -rf docroot/sites/default/files
  ln -s ${MEDIADIR}/drupal-files docroot/sites/default/files
  chown -h www-data:www-data docroot/sites/default/files

  rm -rf docroot/sites/default/private
  ln -s ${MEDIADIR}/drupal-files-private docroot/sites/default/private
  chown -h www-data:www-data docroot/sites/default/private

  # copy latest publish and admin scripts

  cp -f publish_profiles/scripts/autopublish.sh ${HOMEDIR}/
  cp -f publish_profiles/scripts/get_latest_tag.php ${HOMEDIR}/

  # copy publish profile files

  cp -rf publish_profiles/${PROFILE}/* .
  rm -rf publish_profiles

  # composer

  /usr/local/bin/composer install || exit 1

  #create proper drush symlink 

  rm /usr/local/bin/drush
  ln -s ${HOMEDIR}/${TAGNAME}/vendor/drush/drush/drush /usr/local/bin/drush || exit 1

  # Run drush db update and entity update

  cd docroot/sites/default
  /usr/local/bin/drush cr
  /usr/local/bin/drush updb -y || exit 1
  /usr/local/bin/drush entup -y || exit 1
  /usr/local/bin/drush cim sync -y || exit 1

  cd ${HOMEDIR}

  chown -R www-data:www-data ${TAGNAME}

  # link new release and update perms

  rm -f ${DOCROOT}
  ln -s ${TAGNAME}/docroot ${DOCROOT}
  chown www-data:www-data ${TAGNAME}
  chown -h www-data:www-data ${DOCROOT}

  # add crons

  if [ "$PROFILE" == "live" ]; then

    echo "*/10 * * * * cd /var/www/vhosts/www.cazloyd.com; /bin/bash autopublish.sh >> /var/log/autopublish.log 2>&1" >> live_root_cron

    crontab live_root_cron
    rm live_root_cron

  fi

  if [ "$PROFILE" == "staging" ]; then

      echo "*/10 * * * * cd /var/www/vhosts/staging.cazloyd.com; /bin/bash autopublish.sh >> /var/log/autopublish.log 2>&1" >> staging_root_cron

      crontab staging_root_cron
      rm staging_root_cron

    fi

  # restart services

  /usr/sbin/service php7.2-fpm restart

  # remove all but last 10 releases

  if [ "$PROFILE" == "live" ]; then

    find . -type d -iname 'live-*' -printf '%T@\t%p\n' |
    sort -t $'\t' -g |
    head -n -10 |
    cut -d $'\t' -f 2- |
    xargs rm -rf

  fi

  if [ "$PROFILE" == "staging" ]; then

    find . -type d -iname 'staging-*' -printf '%T@\t%p\n' |
    sort -t $'\t' -g |
    head -n -10 |
    cut -d $'\t' -f 2- |
    xargs rm -rf

  fi

  mail -s "$PROJECT Autodeploy" devops@graphicalliance.co.uk <<< "Published ${TAGNAME} on ${PROFILE}"

  echo "Done"

fi
