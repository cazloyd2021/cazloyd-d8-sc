<?php
// allowed prefixes
$prefixes = array('live', 'admin', 'preprod', 'staging', 'release');

if (isset($argv[1]) && isset($argv[2])) {
  
  $prefix = $argv[1];
  
  if (!in_array($prefix, $prefixes)) {
    echo 0;
    exit();
  }
  
  // if ($prefix=='admin') $prefix='live';
  
  $folder = $argv[2];
  
  if (!is_dir($folder)) {
    echo 0;
    exit();
  }
  
  $command = "git -C ${folder} tag -l '${prefix}-[0-9]*.[0-9].[0-9]'";
  $tags = shell_exec($command);
  $tags = trim($tags);
  
  if (strlen($tags)>0) {
  
    $tags = explode("\n", trim($tags));
    sort($tags, SORT_NATURAL);
    $tag = array_pop($tags);
    echo $tag;
    
  } else {
    
    echo 0;
    
  }
  
}

