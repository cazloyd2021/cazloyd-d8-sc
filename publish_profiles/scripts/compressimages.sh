#!/usr/bin/env bash

cd /data/henley/henley-files

find -name "*.jpg" -type f -print0 | xargs -0 jpegoptim -m70
find -name "*.png" -type f -print0 | xargs -0 optipng -o 2
