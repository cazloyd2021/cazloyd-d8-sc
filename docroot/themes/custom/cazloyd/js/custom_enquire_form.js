function validate_signup(frm) {
    var emailAddress = frm.Email.value;
    var errorString = '';
    if (emailAddress === '' || emailAddress.indexOf('@') === -1) {
        errorString += 'Please enter your email address\n';
    }

    var gdprOptIn = document.getElementById('cd_GDPROPTIN');
    if (gdprOptIn.checked === false) {
        errorString += 'Please check the box to indicate you opt-in and have read the Privacy Statement.\n';
    }

    var els = frm.getElementsByTagName('input');
    for (var i = 0; i < els.length; i++)
    {
        if (els[i].className == 'text' || els[i].className == 'date' || els[i].className == 'number')
        {
            if (els[i].value == ''){
                errorString = 'Please complete all required fields.';
            }
        }
        else if (els[i].className == 'checkbox'){
            var toCheckChkBx = document.getElementsByName(els[i].name);
            var checkBoxChecked = false;
            if(toCheckChkBx.checked){
                checkBoxChecked = true;
            }else{
                errorString = 'Please agree to the Privacy Policy and opt in to receive regular newsletter emails from CazeNove Loyd.';
            }
        }
        else if (els[i].className == 'radio')
        {
            var toCheck = document.getElementsByName(els[i].name);
            var radioChecked = false;
            for (var j = 0; j < toCheck.length; j++)
            {
                if (toCheck[j].name == els[i].name && toCheck[j].checked){
                    radioChecked = true;
                }
            }
            if (!radioChecked){
                errorString = 'Please complete all required fields.';
            }

        }
    }



    var isError = false;
    if (errorString.length > 0)
        isError = true;

    if (isError)
        alert(errorString);
    return !isError;
}

/****************************************************************************************************************
 * Get a query string parameter using regex
 * @param name
 * @param url
 * @returns {*}
 * @author John Cogan
 ****************************************************************************************************************/
function getParameterByName(name, url) {
  if (!url){
    url = window.location.href;
  }

  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);

  if (!results){
    return null;
  }

  if (!results[2]){
    return '';
  }

  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/****************************************************************************************************************
 * On DIV item click, set the hidden field storing the selected item for submission and also change the style to
 * highlight the div.
 * @param smallgroupid
 * @param sourceDiv
 * @author John Cogan
 ****************************************************************************************************************/
function setSelectedSmallGroup(smallgroupid, sourceDiv){
    // console.log('CALLING: setSelectedSmallGroup');
    // console.log('Arg: smallgroupid: ' + smallgroupid);
    // console.log('Arg: sourceDiv: ' + sourceDiv);

    // Get the ID of the div that was clicked
    var elementSource = document.getElementById('small-group-' + smallgroupid);

    // Name of CSS class that will be put on the active item
    var activeClassName = 'small-group-selected-div';

    if(elementSource.classList.contains(activeClassName)){
        elementSource.classList.remove(activeClassName);
        updateSelectedSmallGroups(smallgroupid, 'REMOVE');
    }else{
        elementSource.classList.add(activeClassName);
        updateSelectedSmallGroups(smallgroupid, 'ADD');
    }
}

/**
 * Added for multi select functionality on small group section of enquire form
 * @param smallgroupIdValue
 * @param removeAdd
 */
function updateSelectedSmallGroups(smallgroupIdValue, removeAdd){
    // console.log('CALLING: updateSelectedSmallGroups');
    // console.log('Arg: smallgroupIdValue: ' + smallgroupIdValue);
    // console.log('Arg: removeAdd: ' + removeAdd);

    // var element = document.getElementById('selected_small_groups');

    // Get CSV value
    var hiddenElementValue = document.getElementsByName('selected_small_groups')[0].value;
    // console.log('updateSelectedSmallGroups (1): Currently selected small group ids: ' + hiddenElementValue);

    // If CSV string is NOT empty fill array with CSV values
    var elementValueArray = hiddenElementValue.split(",");

    // Action: Add or Remove
    if(removeAdd == 'REMOVE'){
        // Find position in array of item to remove
        var indexOfItemToRemove = elementValueArray.indexOf(smallgroupIdValue);

        // Remove the item at position
        if (indexOfItemToRemove !== -1) elementValueArray.splice(indexOfItemToRemove, 1);
    }else if(removeAdd == 'ADD'){
        // Append item to the array
        elementValueArray.push(smallgroupIdValue);
    }else{
        // unknown `removeAdd` state, do nothing
    }

    // Update the form input with the new CSV (Converted from array)
    var csvy = elementValueArray.join(",");
    // console.log('csv: Currently selected small group ids:' + csvy);

    var x = document.getElementsByName('selected_small_groups')[0];
    x.value = csvy;
    // console.log('updateSelectedSmallGroups (2): Currently selected small group ids:' + x.value);
}

// Hide elements based on what has been selected
document.addEventListener('DOMContentLoaded', function(){

    var budgetsToHideSelectors = {
            group1 : ['#edit-budget-per-person-2500-3500','#edit-budget-per-person-3500-5000'],
            group2 : ['#edit-budget-per-person-2500-3500']
        },
        countriesSelectors = {
            group1 : ['#edit-which-destination-asia-282','#edit-which-destination-asia-286'],
            group2 : ['#edit-which-destination-asia-308','#edit-which-destination-asia-185']
        },
        countriesSelected = {
            group1 : [],
            group2 : []
        },
        countries = {
            group1 : document.querySelectorAll(countriesSelectors.group1.join()),
            group2 : document.querySelectorAll(countriesSelectors.group2.join()),
        },
        budgetsToHide = {
            group1 : document.querySelectorAll(budgetsToHideSelectors.group1.join()),
            group2 : document.querySelectorAll(budgetsToHideSelectors.group2.join())
        },
        countriesNoGroup = null,
        lengthSelect = document.querySelector('#edit-length-of-your-trip'),
        targetLengthSelected = false,
        __logic_class_prefix = 'js-dependency-logic-';


    function runCheck() {
        function hide_group_targets(group_name){
            for (i = 0; i < budgetsToHide[group_name].length; ++i) {
                budgetsToHide[group_name][i].parentNode.parentNode.classList.add('hide-button');
            }
        }
        function show_group_targets(group_name){
            for (i = 0; i < budgetsToHide[group_name].length; ++i) {
                budgetsToHide[group_name][i].parentNode.parentNode.classList.remove('hide-button');
            }
        }
        if ( targetLengthSelected && noneCountryNotOnGroupSelected()){
            if ( anyCountryOnGroupSelected('group1') ) {
                hide_group_targets('group1');
            } else if ( anyCountryOnGroupSelected('group2') ) {
                show_group_targets('group1');
                hide_group_targets('group2');
            } else {
                show_group_targets('group1');
                show_group_targets('group2');
            }
        } else{
            show_group_targets('group1');
            show_group_targets('group2');
        }
    }


    function anyCountryOnGroupSelected(group_name){
        var result = false;
        for(i = 0; i < countries[group_name].length; ++i){
            if( countriesSelected[group_name][i] ) { result = true; break; }
        }
        return result;
    }

    function noneCountryNotOnGroupSelected(){
        var result = true;
        for( i = 0; i < countriesNoGroup.length; i++ ){
            if( countriesNoGroup[i].checked ){ result = false; break; }
        }
        return result;
    }

    function init(){

        function countrySelectHandling(e){
            var group_name = "";
            if ( e.target.classList.contains(__logic_class_prefix+'has-group') ){
                group_name = e.target.getAttribute(__logic_class_prefix+'group');
                if ( e.target.checked ) {
                    countriesSelected[group_name][countriesSelectors[group_name].indexOf('#'+e.target.id)] = true;
                } else {
                    countriesSelected[group_name][countriesSelectors[group_name].indexOf('#'+e.target.id)] = false;
                }
                runCheck();
            }
        }

        for (var group_name in countries) {
            if (countries.hasOwnProperty(group_name)) {
                for (i = 0; i < countries[group_name].length; ++i){
                    countries[group_name][i].setAttribute(__logic_class_prefix+'group', group_name);
                    countries[group_name][i].classList.add(__logic_class_prefix+'has-group');
                    countriesSelected[group_name].push(false);
                    countries[group_name][i].addEventListener("change", countrySelectHandling);
                }
            }
        }

        countriesNoGroup = document.querySelectorAll('#edit-which-destination-asia .form-checkbox:not(.'+__logic_class_prefix+'has-group'+')');
        for (i = 0; i < countriesNoGroup.length; ++i){
            countriesNoGroup[i].addEventListener("change", runCheck);
        }

        if(lengthSelect) {
          lengthSelect.addEventListener("change", function () {
            if ((lengthSelect.value == '1-week') ||
              (lengthSelect.value == '5-days')) {
              targetLengthSelected = true;
              runCheck();
            } else {
              targetLengthSelected = false;
              runCheck();
            }
          });
        }

    }

    init();

});
