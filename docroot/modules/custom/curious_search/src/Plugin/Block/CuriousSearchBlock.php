<?php
namespace Drupal\curious_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

use Drupal\node\Entity\Node;

/**
 * Provides a 'CuriousSearchBlock' Block.
 *
 * @Block(
 *   id = "curioussearchblock",
 *   admin_label = @Translation("Curious Search block"),
 * )
 */
class CuriousSearchBlock extends BlockBase implements BlockPluginInterface {

  /**
    * @return array
  */
  public function build() {

    $builtForm = \Drupal::formBuilder()->getForm('Drupal\curious_search\Form\CuriousSearchForm');
  	$renderArray['form'] = $builtForm;

  	return $renderArray;

  }

}