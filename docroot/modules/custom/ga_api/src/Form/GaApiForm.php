<?php

namespace Drupal\ga_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class GaApiForm.
 *
 * @package Drupal\ga_api\Form
 */
class GaApiForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * Return config name.
   *
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ga_api.admin.settings',
    ];
  }

  /**
   * Return form  name.
   *
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ga_api_form';
  }

  /**
   * Build admin form.
   *
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ga_api.admin.settings');

    $form['ga_api_fieldset_success'] = [
      '#type' => 'details',
      '#title' => $this->t('Send success emails'),
      '#prefix' => '<div id="details-fieldset">',
      '#suffix' => '</div>',
      '#open' => TRUE,
    ];

    $form['ga_api_fieldset_success']['description'] = [
      '#markup' => '<strong>NOTE:</strong> All API errors are logged to watchdog',
    ];

    $form['ga_api_fieldset_success']['success_peak15'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('success_peak15'),
      '#title' => t('Send email for PEAK 15 <strong>successful</strong> API call'),
    ];

    $form['ga_api_fieldset_success']['success_constant_contact'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('success_constant_contact'),
      '#title' => t('Send email for Constant Contact <strong>successful</strong> API call'),
    ];

    $form['ga_api_fieldset_success']['send_on_success_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add emails (comma seperated)'),
      '#default_value' => $config->get('send_on_success_emails'),
      '#size' => 128,
    ];


    $form['ga_api_fieldset_fail'] = [
      '#type' => 'details',
      '#title' => $this->t('Send failure emails'),
      '#prefix' => '<div id="details-fieldset">',
      '#suffix' => '</div>',
      '#open' => TRUE,
    ];

    $form['ga_api_fieldset_fail']['description'] = [
      '#markup' => '<strong>NOTE:</strong> All API errors are logged to watchdog',
    ];

    $form['ga_api_fieldset_fail']['fail_peak15'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('fail_peak15'),
      '#title' => t('Send email for PEAK 15 <strong>failure</strong> on API call'),
    ];

    $form['ga_api_fieldset_fail']['fail_constant_contact'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('fail_constant_contact'),
      '#title' => t('Send email for Constant Contact <strong>failure</strong> on API call'),
    ];

    $form['ga_api_fieldset_fail']['send_on_fail_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add emails (comma seperated)'),
      '#default_value' => $config->get('send_on_fail_emails'),
      '#size' => 128,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit form.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    if ($form['#id'] == 'ga-api-form') {

      $this->config('ga_api.admin.settings')
        ->set('success_peak15', $form_state->getValue('success_peak15'))
        ->set('success_constant_contact', $form_state->getValue('success_constant_contact'))
        ->set('send_on_success_emails', $form_state->getValue('send_on_success_emails'))
        ->set('fail_peak15', $form_state->getValue('fail_peak15'))
        ->set('fail_constant_contact', $form_state->getValue('fail_constant_contact'))
        ->set('send_on_fail_emails', $form_state->getValue('send_on_fail_emails'))
        ->save();
    }
  }

}
