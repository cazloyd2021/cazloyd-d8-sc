(function ($) {
  // Disable browser autofill on Postcode field to allow AddressLookup be triggered
  // https://graphicalliance.atlassian.net/browse/CLSR20-45
  $('#edit-i-would-like-to input').on('click',function(){
    $('#edit-address-postal-code').attr('autocomplete','new-password');
  });
})(jQuery);
