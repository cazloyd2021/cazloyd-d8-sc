<?php

namespace Drupal\instagram_import\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Url;
use GuzzleHttp\Client;

/**
 * Provides route responses for the Example module.
 */
class GetTokenInstagram extends ControllerBase {
  
  /**
   * Request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  public $request;
  
  /**
   * Class constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request stack.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }
  
  /**
   * Returns a callback page for creation Linkedin token.
   *
   * @return array
   *   A simple render array.
   */
  public function getToken() {
    
    $config = \Drupal::configFactory()
      ->getEditable('instagram_import.settings');
    
    $code = $this->request->getCurrentRequest()->get('code');
    $error = $this->request->getCurrentRequest()->get('error');
    $error_description = $this->request->getCurrentRequest()
      ->get('error_description');
    
    if ($code) {
      $client_id = $config->get('consumer_key');
      $client_secret = $config->get('consumer_secret');
      
      $httpClient = new Client();
      
      $response = $httpClient->request('POST',
        'https://api.instagram.com/oauth/access_token', [
          'form_params' => [
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'authorization_code',
            'redirect_uri' => Url::fromUri('base:admin/config/content/instagram_import/token',
              ['absolute' => TRUE])->toString(),
            'code' => $code,
          ],
        ]);
      
      $body = (string) $response->getBody();
      $body = json_decode($body);
      
      if (is_object($body)) {
        
        $config->set('token', $body->access_token);
        $config->save();
        
        drupal_set_message($this->t('New Instagram access token has been generated.'),
          'status');
        $url = Url::fromRoute('instagram_import.config_form');
        $response = new RedirectResponse($url->toString());
        $response->send();
  
        $result = new JsonResponse(['status' => 'ok']);
        return $result;
        
      }
      else {
        
        \Drupal::logger('instagram_import')
          ->error($response->getBody());
        
        drupal_set_message($this->t('Error generating Instagram access token.'),
          'error');
        $url = Url::fromRoute('instagram_import.config_form');
        $response = new RedirectResponse($url->toString());
        $response->send();
  
        $result = new JsonResponse(['status' => 'ok']);
        return $result;
        
      }
      
    }
    elseif ($error) {
      \Drupal::logger('instagram_import')
        ->error($error_description);
      
      drupal_set_message($this->t('Error generating Instagram access token.'),
        'error');
      $url = Url::fromRoute('instagram_import.config_form');
      $response = new RedirectResponse($url->toString());
      $response->send();
  
      $result = new JsonResponse(['status' => 'ok']);
      return $result;
    }
  }
  
}
