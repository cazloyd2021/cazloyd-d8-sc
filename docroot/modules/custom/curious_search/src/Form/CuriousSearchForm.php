<?php
/**
 * @file
 * Contains \Drupal\curious_search\Form\CuriousSearchForm.
 */

namespace Drupal\curious_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;

use Drupal\Core\Url;

//use Drupal\node\Entity\Node;

//use Drupal\Core\Routing\TrustedRedirectResponse;
//use Symfony\Component\HttpFoundation\RedirectResponse;



/**
 * Curious finder form.
 */
class CuriousSearchForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_curious_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['curious_search'] = array(
      '#type' => 'entity_autocomplete',
      '#title' => $this->t("Search Curious"),
      '#attributes' => array(
        'placeholder' => array('Type in & press enter'),
      ),
      '#target_type' => 'node',
      '#selection_handler' => 'default',
      '#selection_settings' => array(
        'target_bundles' => array('blog'),
      ),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Find'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $curious_search_id = $form_state->getValue('curious_search');

    if(isset($curious_search_id) && !empty($curious_search_id) && is_numeric($curious_search_id)){
      $goto_curiouspage = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$curious_search_id);
    } else {
      $goto_curiouspage = '';
    }


    /**************************************************************
    Different methods to redirect
    **************************************************************/

    if(isset($goto_curiouspage) && !empty($goto_curiouspage)){
      //$url = Url::fromUri('internal:' . $goto_curiouspage);
      //$form_state->setRedirectUrl( $url );

      //$redirect_url = Url::fromUri('entity:node/'.$curious_search_id);
      //return new RedirectResponse($redirect_url->toString(), 301);


      //$url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $curious_search_id]);
      //$form_state->setRedirectUrl($url);

      //$redirect_url = Url::fromUri('entity:node/'.$curious_search_id);
      //$response = new RedirectResponse($redirect_url->toString(), 301);
      //$form_state->setResponse($response);
      //$form_state->setRedirectUrl(Url::fromUserInput('/node/19'));

      /******************************************
      TAKES YOU TO ALL
      *******************************************/
      /*global $base_url;
      $full_url = $base_url.$goto_curiouspage;
      $response = new TrustedRedirectResponse($full_url);

      $options = ['absolute' => TRUE];
      $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $curious_search_id], $options);
      $form_state->setRedirectUrl($url);

      $form_state->setResponse($response);
      */

      /********************************************
      TAKES YOU TO ALL
      ********************************************/
      //$form_state->setRedirect('entity.node.canonical', array('node' => $curious_search_id), $options);


      /********************************************
      ALSO TAKES YOU TO ALL
      ********************************************/
      //$options = ['absolute' => TRUE];
      //$url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $curious_search_id], $options);
      //$form_state->setRedirectUrl($url);



      /*******************************************
      method also takes you to All
      ********************************************/
      //$redirect_path = "/node/".$curious_search_id;
      //$url = url::fromUserInput($redirect_path);
      //$form_state->setRedirectUrl($url);



      /**********************************************
      method takes you to All
      ***********************************************/
      // if(isset($goto_curiouspage) && !empty($goto_curiouspage)){
      //   $url = Url::fromUri('internal:' . $goto_curiouspage);  
      //   $form_state->setRedirectUrl( $url );
      // } 



      /****************************************************
      working method below - not right way to do it
      ****************************************************/
      global $base_url;
      header ('Location:' . $base_url.$goto_curiouspage);
      die();
    }
  }

}
?>