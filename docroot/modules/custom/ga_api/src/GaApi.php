<?php

namespace Drupal\ga_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\key\KeyRepository;
use Drupal\node\Entity\Node;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\BadResponseException;
use phpDocumentor\Reflection\Types\Boolean;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GaApi.
 *
 * Handles the sending of webform/form submissions to external API.
 *
 * @package Drupal\ga_api
 */
class GaApi {

  /**
   * Constant contact create url.
   *
   * @var string
   */
  const CONSTANT_CONTACT_CREATE_URL = 'https://api.constantcontact.com/v2/contacts?action_by=ACTION_BY_VISITOR';

  /**
   * Constant Peak 15 inquiry url.
   *
   * @var string
   */
  const PEAK_15_INQUIRY_URL = 'https://data.peak15systems.com/beacon/service.svc/insert/complex/contactinquiry';

  /**
   * Constant contact Website sign up list id.
   *
   * @var string
   */
  const LIST_ID = '1371986373';

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client to fetch data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The key repository service.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * GaApi constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \GuzzleHttp\ClientInterface $http_client
   * @param \Drupal\key\KeyRepository $key_repository
   * @param \Psr\Log\LoggerInterface $logger
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    KeyRepository $key_repository,
    LoggerInterface $logger
  ) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->keyRepository = $key_repository;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('key.repository'),
      $container->get('logger.factory')->get('ga_api')
    );
  }

  /**
   * Get headers for authorization.
   *
   * @return string[]|null
   */
  public function getAuthHeaders() {
    // Get token
    $token = $this->getAccessToken();

    if ($token) {
      return [
        //'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $token,
      ];
    }
    else {
      return NULL;
    }
  }

  /**
   * Get Peak 15 Access token.
   *
   * @return mixed|string|null
   */
  private function getPeak15AccessToken() {
    // Check for Access token.
    $getAccessToken = $this->keyRepository->getKey('peak_15_access_token');

    // If token exists.
    if ($getAccessToken) {
      return $getAccessToken->getKeyValue();
    }
    // No token found, log error.
    else {
      $this->logError(['function' => __FUNCTION__], 'Unable to get access token: peak_15_access_token.key');
      return NULL;
    }
  }

  /**
   * Get Access token.
   *
   * @return mixed|string|null
   */
  private function getAccessToken() {
    // Check for Access token.
    $getAccessToken = $this->keyRepository->getKey('constant_contact_access_token');

    // If token exists.
    if ($getAccessToken) {
      return $getAccessToken->getKeyValue();
    }
    // No token found, log error.
    else {
      $this->logError(['function' => __FUNCTION__], 'Unable to get access token: constant_contact_access_token.key');
      return NULL;
    }
  }

  /**
   * Get API Key.
   *
   * @return mixed|string|null
   */
  public function getApiKey() {
    // Check for API key.
    $getApiKey = $this->keyRepository->getKey('constant_contact_api_key');

    // If key exists.
    if ($getApiKey) {
      return $getApiKey->getKeyValue();
    }
    // No key found, log error.
    else {
      $this->logError(['function' => __FUNCTION__], 'Unable to get API key: constant_contact_api_key.key');
      return NULL;
    }
  }

  /**
   * Method to send data to Constant contact.
   *
   * @param array $webformValues
   */
  public function sendConstantContact(array $webformValues) {
    // Build post fields.
    $contactArray= [];

    $contactArray['lists'][]['id'] = self::LIST_ID;
    $contactArray['email_addresses'][]['email_address'] = $webformValues['email_address'];

    if ($webformValues['first_name']) {
      $contactArray['first_name'] = $webformValues['first_name'];
    }
    if ($webformValues['last_name']) {
      $contactArray['last_name'] = $webformValues['last_name'];
    }

    // Make API call.
    $this->callApi('POST', self::CONSTANT_CONTACT_CREATE_URL, $contactArray, ['function' => __FUNCTION__]);
  }

  /**
   * Method to send data to Peak 15.
   *
   * @param array $webformValues
   */
  public function sendPeak15(array $webformValues) {
    // Load webform to get webform elements.
    $webform = \Drupal::entityTypeManager()->getStorage('webform')->load('enquire');
    $webformElements = $webform->getElementsDecodedAndFlattened();

    // Build post fields.
    $contactArray= [];

    // Peak 15 token.
    $contactArray['token'] = $this->getPeak15AccessToken();

    // contact.firstname.
    $contactArray['contact.firstname'] = $webformValues['first_name'];

    // contact.lastname.
    $contactArray['contact.lastname'] =  $webformValues['last_name'];

    // contact.emailaddress1
    $contactArray['contact.emailaddress1'] = $webformValues['email_address'];

    // contact.cazloyd_preferrednumber
    $contactArray['contact.cazloyd_preferrednumber'] = $webformValues['telephone_number'] ;

    // contact.rule;
    $contactArray['contact.rule'] = $webformValues['first_name'] . ' ' . $webformValues['last_name'] .' '. $webformValues['email_address'];

    // Static values needed for Peak 15.
    $contactArray['contact.p15_contacttype_contactid'] = 'Prospect';
    $contactArray['p15_inquiries.p15_channel'] = 'Web';

    // p15_inquiries.cazloyd_typeoftrip.
    $typeOfTrip = array();
    $typeOfTrip['celebration'] = 'Celebration';
    $typeOfTrip['small-group-travel'] = 'Small Group Travel';
    $typeOfTrip['tailor-made-holiday'] = 'Tailor-made';

    if (!empty($webformValues['i_would_like_to'])) {
      $contactArray['p15_inquiries.cazloyd_typeoftrip'] = $typeOfTrip[$webformValues['i_would_like_to']];
    }
    else {
      $contactArray['p15_inquiries.cazloyd_typeoftrip'] = $typeOfTrip['small-group-travel'];
    }
    // p15_inquiries.p15_name.
    $countriesString = '';

    if (!empty($webformValues['which_destination_asia'])) {
      foreach ($webformValues['which_destination_asia'] as $key) {
        $countriesString .= $webformElements['which_destination_asia']['#options'][$key] . ', ';
      }
    }

    if (!empty($webformValues['which_destination_africa'])) {
      foreach ($webformValues['which_destination_africa'] as $key) {
        $countriesString .= $webformElements['which_destination_africa']['#options'][$key] . ', ';
      }
    }

    if (!empty($webformValues['which_destinations_middle_east'])) {
      foreach ($webformValues['which_destinations_middle_east'] as $key) {
        $countriesString .= $webformElements['which_destinations_middle_east']['#options'][$key] . ', ';
      }
    }

    if (!empty($webformValues['which_destination_central_america'])) {
      foreach ($webformValues['which_destination_central_america'] as $key) {
        $countriesString .= $webformElements['which_destination_central_america']['#options'][$key] . ', ';
      }
    }

    if (!empty($webformValues['which_destination_polar'])) {
      foreach ($webformValues['which_destination_polar'] as $key) {
        $countriesString .= $webformElements['which_destination_polar']['#options'][$key] . ', ';
      }
    }

    if (!empty($webformValues['which_destination_australasia'])) {
      foreach ($webformValues['which_destination_australasia'] as $key) {
        $countriesString .= $webformElements['which_destination_australasia']['#options'][$key] . ', ';
      }
    }

    if ($countriesString !='') {
      $contactArray['p15_inquiries.p15_name'] = $webformValues['last_name'] .' - '. substr($countriesString,0, -2);
    }
    else {
      $contactArray['p15_inquiries.p15_name'] = $webformValues['last_name'];
    }

    // p15_inquiries.cazloyd_regionofinterest.
    $regionOfInterest = array();
    $regionOfInterest['africa-indian-ocean'] = 'Africa + Indian Ocean';
    $regionOfInterest['asia'] = 'Asia';
    $regionOfInterest['australasia'] = 'Australasia';
    $regionOfInterest['central-south-america'] = 'Central + South America';
    $regionOfInterest['europe'] = 'Europe';
    $regionOfInterest['middle-east'] = 'The Middle East';
    $regionOfInterest['polar-region'] = 'The Polar Regions';
    $regionOfInterest['north-america-alaska'] = 'North America + Alaska';
    $regionOfInterest['inspire-me'] = 'Inspire me';

    if (!empty($webformValues['region_buttons'])) {
      $contactArray['p15_inquiries.cazloyd_regionofinterest'] = $regionOfInterest[$webformValues['region_buttons']];
    }

    // p15_inquiries.cazloyd_monthoftravel.
    if (!empty($webformValues['when_would_you_like_to_travel_'])) {
      $contactArray['p15_inquiries.cazloyd_monthoftravel'] = $webformValues['when_would_you_like_to_travel_'];
    }

    // p15_inquiries.cazloyd_inwhichyeartotravel.
    if (!empty($webformValues['in_which_year_would_you_like_to_travel_'])) {
      $contactArray['p15_inquiries.cazloyd_inwhichyeartotravel'] = $webformValues['in_which_year_would_you_like_to_travel_'];
    }

    // p15_inquiries.cazloyd_paxtravelling.
    $peopleTravelling = array();
    $peopleTravelling['1'] = '1';
    $peopleTravelling['2'] = '2';
    $peopleTravelling['3'] = '3';
    $peopleTravelling['4'] = '4';
    $peopleTravelling['5'] = '5';
    $peopleTravelling['6'] = '6';
    $peopleTravelling['7'] = '7';
    $peopleTravelling['8'] = '8';
    $peopleTravelling['9'] = '9';
    $peopleTravelling['10-plus'] = '10+';
    $peopleTravelling['Unsure'] = 'Unsure';

    if (!empty($webformValues['how_many_people_will_be_travelling_'])) {
      $contactArray['p15_inquiries.cazloyd_paxtravelling'] = $peopleTravelling[$webformValues['how_many_people_will_be_travelling_']];
    }

    // p15_inquiries.cazloyd_lengthoftrip.
    $lengthOfTrip = array();
    $lengthOfTrip['five-nights'] = 'Five nights';
    $lengthOfTrip['one-week'] = 'One week';
    $lengthOfTrip['10-nights'] = '10 nights';
    $lengthOfTrip['Two-weeks'] = 'Two weeks';
    $lengthOfTrip['three-weeks-plus'] = 'Three weeks+';

    if (!empty($webformValues['length_of_your_trip'])) {
      $contactArray['p15_inquiries.cazloyd_lengthoftrip'] = $lengthOfTrip[$webformValues['length_of_your_trip']];
    }

    // p15_inquiries.cazloyd_tellusmore.
    if (!empty($webformValues['tell_us_more_about_the_holiday_you_want'])) {
      $contactArray['p15_inquiries.cazloyd_tellusmore'] = trim($webformValues['tell_us_more_about_the_holiday_you_want']);
    }
    elseif (!empty($webformValues['about_the_holiday_you_want'])) {
      $contactArray['p15_inquiries.cazloyd_tellusmore'] = trim($webformValues['about_the_holiday_you_want']);
    }

    // p15_inquiries.cazloyd_budget.
    $budget = array();
    $budget['£2,500 - 3,500'] = '£2,500-£3,500';
    $budget['£3,500 - 5,000'] = '£3,500-£5,000';
    $budget['£5,000 - 7,500'] = '£5,000-£7,500';
    $budget['£7,500 - 10,000'] = '£7,500-£10,000';
    $budget['£10,000 +'] = '£10,000+';

    if (!empty($webformValues['budget_per_person'])) {
      $contactArray['p15_inquiries.cazloyd_budget'] = $budget[$webformValues['budget_per_person']];
    }

    // p15_inquiries.cazloyd_address.
    if (!empty($webformValues['address'])) {
      $address = '';
      foreach ($webformValues['address'] as $add) {
        if ($add != '') {
          $address .= $add . ', ';
        }
      }

      $contactArray['p15_inquiries.cazloyd_address'] = substr($address,0, -2);
    }

    // p15_inquiries.cazloyd_enquirysource.
    $enquirySource = array();
    $enquirySource['magazine-newspaper-or-article'] = 'A magazine, newspaper or article';
    $enquirySource['marketing-email'] = 'Received a marketing email from you';
    $enquirySource['Referred-by-family-or-friend'] = 'Referred by family / a friend';
    $enquirySource['received-curious-magazine-in-post'] = 'Received CURIOUS magazine in the post';
    $enquirySource['Online search'] = 'Found you by searching online';
    $enquirySource['social-media'] = 'Social media';
    $enquirySource['through-another-company'] = 'Through another company';
    $enquirySource['attended-an-event'] = 'Attended an event';
    $enquirySource['_other_'] = 'Other';

    if (!empty($webformValues['how_did_you_hear_about_us_or_what_prompted_you_to_get_in_touch_'])) {
      if (!array_key_exists($webformValues['how_did_you_hear_about_us_or_what_prompted_you_to_get_in_touch_'], $enquirySource)) {
        $contactArray['p15_inquiries.cazloyd_enquirysource'] = $enquirySource['_other_'];
      }
      else {
        $contactArray['p15_inquiries.cazloyd_enquirysource'] = $enquirySource[$webformValues['how_did_you_hear_about_us_or_what_prompted_you_to_get_in_touch_']];
      }
    }
    elseif (!empty($webformValues['how_did_you_hear_about_us_'])) {
      $contactArray['p15_inquiries.cazloyd_enquirysource'] = $enquirySource[$webformValues['how_did_you_hear_about_us_']];
    }

    // p15_destinations.
    if ($countriesString != '') {
      $splitCountryString = explode(', ', $countriesString);

      $x = 1;
      foreach ($splitCountryString as $split) {
        if ($split != '') {
          $contactArray['p15_destinations.' . $x] = $split;
          $x++;
        }
      }
    }

    // p15_inquiries.cazloyd_whichsmallgrouptrip.
    if (!empty($webformValues['selected_small_groups'])) {

      $contactArray['p15_inquiries.cazloyd_whichsmallgrouptrip'] = $webformValues['selected_small_groups'];
    }
    elseif (!empty($webformValues['small_group_trip_checkboxes'])) {
      $small_group_trip_checkboxes = $webformValues['small_group_trip_checkboxes'];
      $nodes = Node::loadMultiple($small_group_trip_checkboxes);
      $p_15_small_group_trip_string = '';
      foreach ($nodes as $key => $node) {
        $label = $node->label();
        $start_date = $node->get('field_start_date')->value;
        $end_date = $node->get('field_end_date')->value;
        $p_15_small_group_trip_string .= $label . '(' . $start_date . ' - ' . $end_date . ');';
      }

      $contactArray['p15_inquiries.cazloyd_whichsmallgrouptrip'] = $p_15_small_group_trip_string;
    }
    elseif (!empty($webformValues['country'])) {
      $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($webformValues['country']);
      $contactArray['p15_inquiries.cazloyd_whichsmallgrouptrip'] = $term->label();
    }

//    echo '<pre>';
//    print_r($contactArray);
//    print_r($webformValues);
    //exit;

    // Make API call.
    $this->callApi('GET', self::PEAK_15_INQUIRY_URL, $contactArray, ['function' => __FUNCTION__],  1);
  }

  /**
   * Make call to API.
   *
   * @param string $method
   *   The HTTP method for the call.
   * @param string $url
   *   The API endpoint.
   * @param array $options
   *   Request options passed to the HTTP client.
   * @param array $caller
   *   Associative array of data about the caller of this method. e.g.
   *   function, payload etc.
   * @param array $isPeak15
   *  Bool to determine if call is peak 15 or constant contact.
   *
   * @return mixed
   *   The response data from the API.
   */
  public function callApi($method, $url, array $options, array $caller, $isPeak15 = NULL) {

    $config = \Drupal::config('ga_api.admin.settings');

    if ($isPeak15 != 1) {
      // Get API key.
      $apiKey = $this->getApiKey();

      // Get Headers.
      $getHeaders = $this->getAuthHeaders();
    }
    else {
      $apiKey = 1;
      $getHeaders = 1;
    }

    if (($apiKey != NULL) && ($getHeaders != NULL)) {
      $data = NULL;
      try {

        // Constant contact API call.
        if ($isPeak15 != 1) {
          $url = 'https://api.constantcontact.com/v2/contacts';
          $email_address_value = $options['email_addresses'][0]['email_address'];
          $email_address_value = rawurlencode($email_address_value);

          // Make request and retrieving a list of exist contacts.
          $uri = $url . '?' . 'email=' . "$email_address_value" . '&status=ALL' . '&limit=1' . '&api_key=' . $apiKey;
          $response = $this->httpClient->request('GET', $uri, ['headers' => $getHeaders, 'body' => '']);
          $response_result = json_decode($response->getBody()->getContents())->results;

          if (!empty($response_result)) {
            // Make request and modify a particular contact.
            $id = $response_result[0]->id;
            $uri = $url . '/' . "$id" . '?' . 'action_by=ACTION_BY_VISITOR' . '&api_key=' . $apiKey;
            $response = $this->httpClient->request('PUT', $uri, ['headers' => $getHeaders,'body' => json_encode($options, TRUE)]);
          }
          else {
            // Make request and add a Contact to a collection.
            $uri = $url . '?' . 'action_by=ACTION_BY_VISITOR' . '&api_key=' . $apiKey;
            $response = $this->httpClient->request('POST', $uri, ['headers' => $getHeaders,'body' => json_encode($options, TRUE)]);
          }
        }
        // Peak15 API call.
        else {
          $response = $this->httpClient->request($method, $url, ['query' => $options]);
        }
        $data = $response->getBody()->getContents();

        // For peak 15 api calls, a valid response of creating a user is a returned var in the format '8c53343a-e309-eb11-80da-00155d02b546'
        // Could not use status code to determine if curl request was a pass (200) as curl requests that fail are still coming back as a pass (200)
        if ($isPeak15 == 1) {

          preg_match('/[a-z0-9].{7}-[a-z0-9].{3}-[a-z0-9].{3}-[a-z0-9].{3}-[a-z0-9].{11}/', $data, $preg_match_result);

          // No match = fail
          if ((!isset($preg_match_result[0])) || ($preg_match_result[0] == '')) {
            $this->logError($caller, $data);

            // PEAK 15 Send emails on failure.
            if ($config->get('fail_peak15') == 1) {
              $this->sendGaMail($caller, $data, $options, $isPeak15, 'failure', $config->get('send_on_fail_emails'));
            }
          }
          // Match = success.
          else {
            // PEAK 15 Send emails on success.
            if ($config->get('success_peak15') == 1) {
                $this->sendGaMail($caller, $data, $options, $isPeak15, 'success', $config->get('send_on_success_emails'));
            }
          }
        }
        // Constant contact.
        else {
          // Send emails on success.
          if ($config->get('success_constant_contact') == 1) {
                $this->sendGaMail($caller, $data, $options, $isPeak15, 'success', $config->get('send_on_success_emails'));
          }
        }
      } catch (BadResponseException $ex) {
        $data = $ex->getResponse()->getBody()->getContents();
        $this->logError($caller, $data);

        // PEAK 15 Send emails on failure.
        if ($isPeak15 == 1) {
          if ($config->get('fail_peak15') == 1) {
            $this->sendGaMail($caller, $data, $options, $isPeak15, 'failure', $config->get('send_on_fail_emails'));
          }
        }
        // Constant Contact Send emails on failure.
        else {
          if ($config->get('fail_constant_contact') == 1) {
            $this->sendGaMail($caller, $data, $options, $isPeak15, 'failure', $config->get('send_on_fail_emails'));
          }
        }
      } catch (GuzzleException $e) {

        $data = ['code' => $e->getCode(), 'message' => $e->getMessage()];
        $this->logError($caller, $data);

        // PEAK 15 Send emails on failure.
        if ($isPeak15 == 1) {
          if ($config->get('fail_peak15') == 1) {
            $this->sendGaMail($caller, $data, $options, $isPeak15, 'failure', $config->get('send_on_fail_emails'));
          }
        }
        // Constant Contact Send emails on failure.
        else {
          if ($config->get('fail_constant_contact') == 1) {
            $this->sendGaMail($caller, $data, $options, $isPeak15, 'failure', $config->get('send_on_fail_emails'));
          }
        }
      }
    }
  }

  /**
   * Log Errors.
   *
   * @param array $caller
   * @param $response
   */
  public function logError(array $caller, $response) {
    $this->logger->error("METHOD (%function): %response", [
      '%function' => $caller['function'],
      '%response' => $response,
    ]);
  }

  /**
   * Send mail to GA.
   *
   * @param string $caller
   *   The HTTP method for the call.
   * @param array $data
   *   The API response.
   * @param array $options
   *   Request options passed to the HTTP client.
   * @param boolean $isPeak15
   *  Bool to determine if call is peak 15 or constant contact.
   * @param string $messageType
   *   Whether the API call was a sucess or failure.
   * @param string $emails
   *   Email addresses to send to.
   */
  public function sendGaMail($caller, $data, $options, $isPeak15 = NULL, $messageType, $emails) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'ga_api';
    $key = 'send_to_ga';
    $to = $emails;

    $params['mail_title'] = ($messageType == 'success')? 'Webform API submission success' : 'Webform API submission failure';

    $apiType = ($isPeak15 == 1)? 'PEAK 15' : 'Constant contact';

    if ($messageType ==  'success') {
      $message = 'Sucessful API call from Webform submission';
      $message .= '<hr>';
      $message .= '<br> API = ' . $apiType;
      $message .= '<br> Method = ' . json_encode($caller);
      $message .= '<br> CRUL response = ' . json_encode($data);

      if ($isPeak15 == 1) {
        $message .= '<br> PEAK15 link  = ' . 'https://data.peak15systems.com/beacon/service.svc/get/cazloyd/entity/p15_inquiries?token=BbExBvt5RUqEwgYgcQ3GJqCwnrhdBJHcN7LLsL4Mz8NGYr4rMpWehSVnvTDUcjjv&p15_contactinquiriesid=' . $data;
      }

      $message .= '<br> WEBFORM data = ' . json_encode($options);
      $params['message'] = $message;
    }
    else {
      $message = 'There was an error sending data to an API from Webform submission';
      $message .= '<hr>';
      $message .= '<br> API = ' . $apiType;
      $message .= '<br> WATCHDOG = The error has aslo been logged in watchdog';
      $message .= '<br> Method = ' . json_encode($caller);
      $message .= '<br> CRUL response = ' . json_encode($data);
      $message .= '<br> WEBFORM data = ' . json_encode($options);
      $params['message'] = $message;
    }
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = true;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, null, $send);
  }
}
