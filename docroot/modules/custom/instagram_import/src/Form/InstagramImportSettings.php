<?php

namespace Drupal\instagram_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Build Instagram Import settings form.
 */
class InstagramImportSettings extends ConfigFormBase {
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_import_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['instagram_import.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('instagram_import.settings');
    
    $form['#attached']['library'][] = 'instagram_import/authorise_instagram';

    $form['import'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Imports'),
      '#default_value' => $config->get('import'),
    ];

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Post count'),
      '#default_value' => $config->get('count'),
      '#min' => 1,
      '#max' => 200,
    ];

    $intervals = [604800, 2592000, 7776000, 31536000];
    $form['expire'] = [
      '#type' => 'select',
      '#title' => $this->t('Delete old posts'),
      '#default_value' => $config->get('expire'),
      '#options' => [0 => $this->t('Never')] + array_map([\Drupal::service('date.formatter'), 'formatInterval'], array_combine($intervals, $intervals)),
    ];

    $form['oauth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('OAuth Settings'),
      '#description' => $this->t('To enable OAuth based access for Instagram, you must <a href="@url">register your client</a> with Instagram and add the provided credentials here.', ['@url' => 'https://www.instagram.com/developer/clients/manage/']),
    ];

    $form['oauth']['callback_url'] = [
      '#type' => 'item',
      '#title' => $this->t('Callback URL'),
      '#markup' => Url::fromUri('base:admin/config/content/instagram_import/token', ['absolute' => TRUE])->toString(),
    ];

    $form['oauth']['consumer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Client ID'),
      '#default_value' => $config->get('consumer_key'),
      '#required' => TRUE,
    ];

    $form['oauth']['consumer_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Client secret'),
      '#default_value' => $config->get('consumer_secret'),
      '#required' => TRUE,
    ];
  
    $form['oauth']['token'] = [
      '#type' => 'item',
      '#title' => $this->t('OAuth Token'),
      '#markup' => ($config->get('token') ? $config->get('token') : 'n/a'),
    ];
  
    $form['oauth']['authenticate'] = [
      '#type' => 'item',
      '#title' => $this->t('Authorise'),
      '#markup' => '
          <p>Save the form values before authorising:</p>
          <a href="https://api.instagram.com/oauth/authorize/?client_id=CLIENT-ID&redirect_uri=REDIRECT-URI&response_type=code" class="instagram_auth_link">Click here to authorise your client</a><br>
        ',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();

    $this->config('instagram_import.settings')
      ->setData($values)
      ->save();

    drupal_set_message($this->t('Changes saved.'));
  }

}
