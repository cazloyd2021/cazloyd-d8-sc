#!/usr/bin/env bash

# dump database
mysqldump -h cazedb.cn1umotlyw2w.eu-west-1.rds.amazonaws.com -u cazloyd_prod -p cazloyd_drupal_production > databases/cazloyd_drupal_production.sql

# sync database
rsync -avzu databases ubuntu@ip-172-31-40-174.eu-west-1.compute.internal:/var/www/vhosts/staging.cazloyd.com/

# sync files
rsync -avzu drupal-files ubuntu@ip-172-31-40-174.eu-west-1.compute.internal:/var/www/vhosts/staging.cazloyd.com/
rsync -avzu drupal-files-private ubuntu@ip-172-31-40-174.eu-west-1.compute.internal:/var/www/vhosts/staging.cazloyd.com/