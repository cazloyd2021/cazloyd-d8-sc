// Gruntfile based on https://www.taniarascia.com/getting-started-with-grunt-and-sass/

// Load Grunt
module.exports = function (grunt) {
  grunt.initConfig({

    // Tasks
    sass: { // Begin Sass Plugin
      dist: {
        options: {
          trace: true
        },
        files: [{
          expand: true,
          cwd: 'scss',
          src: ['**/*.scss'],
          dest: 'css',
          ext: '.css'
      }]
      }
    },
    postcss: { // Begin Post CSS Plugin
      options: {
        processors: [
          require('autoprefixer')({
            browsers: ['last 2 versions']
          })
        ]
      },
      dist: {
        src: 'css/style.css'
      }
    },
    cssmin: { // Begin CSS Minify Plugin
      target: {
        files: [{
          expand: true,
          cwd: 'css',
          src: ['*.css', '!*.min.css'],
          dest: 'css',
          ext: '.css'
        }]
      }
    },
    uglify: { // Begin JS Uglify Plugin
      build: {
        src: ['js/*.js', '!js/custom_enquire_form.js', '!js/scripts.min.js'],
        dest: 'js/scripts.min.js'
      }
    },
    watch: { // Compile everything into one task with Watch Plugin
      css: {
        files: '**/*.scss',
        tasks: ['sass', 'postcss']
      },
      js: {
        files: ['js/*.js', '!js/custom_enquire_form.js', '!js/scripts.min.js'],
        tasks: ['uglify']
      }
    }
  });
  // Load Grunt plugins
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Register Grunt tasks
  grunt.registerTask('default', ['watch']);
};
