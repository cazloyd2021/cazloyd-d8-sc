<?php

namespace Drupal\dotmailer\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\Field\FieldType\WebformEntityReferenceItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminSettings.
 *
 * @package Drupal\dotmailer\Form
 */
class AdminSettings extends FormBase {

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * AdminSettings constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory->getEditable('dotmailer.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dotmailer_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['additional_settings'] = ['#type' => 'vertical_tabs'];

    $form['settings'] = [
      '#title' => $this->t('Connectivity'),
      '#type' => 'details',
      '#group' => 'additional_settings',
    ];

    $form['settings']['dotmailer_baseurl'] = [
      '#title' => $this->t('dotmailer Base URL'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->configFactory->get('dotmailer_baseurl'),
      '#description' => $this->t('Enter the dotmailer base url for this site.'),
    ];

    // Credential section.
    $form['credential'] = [
      '#title' => $this->t('Credential'),
      '#type' => 'details',
      '#group' => 'additional_settings',
    ];

    $form['credential']['dotmailer_username'] = [
      '#title' => $this->t('dotmailer username'),
      '#type' => 'textfield',
      '#required'=>TRUE,
      '#default_value' => $this->configFactory->get('dotmailer_username'),
      '#description' => $this->t('To enable dotmailer.'),
    ];

    $form['credential']['dotmailer_password'] = [
      '#title' => $this->t('dotmailer password'),
      '#type' => 'password',
      '#required'=>TRUE,
      '#default_value' => $this->configFactory->get('dotmailer_password'),
      '#description' => $this->t('To enable dotmailer.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => ('Save Configuration'),
    ];

    return $form;
  }

  /**
   * Submit handler of admin config form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->set('dotmailer_baseurl', $form_state->getValue('dotmailer_baseurl'))
      ->set('dotmailer_username', $form_state->getValue('dotmailer_username'))
      ->set('dotmailer_password', $form_state->getValue('dotmailer_password'))
      ->save();

    drupal_set_message($this->t('The configuration options have been saved.'));
  }
}