(function ($, Drupal, drupalSettings) {

  $(".instagram_auth_link").click(function () {
    var clientId = $('#edit-consumer-key').val();
    var redirectUrl = document.getElementById('edit-callback-url').textContent.replace('Callback URL', '').trim();
    var authUrl = 'https://api.instagram.com/oauth/authorize/?client_id=' + encodeURIComponent(clientId) + '&redirect_uri=' + encodeURIComponent(redirectUrl) + '&response_type=code';
    $('.instagram_auth_link').prop('href', authUrl);
  });

})(jQuery, Drupal, drupalSettings)