(function ($) {
  
  //used for autosubmit select value
  Drupal.behaviors.curioussearchFilterAutosubmit = {
    attach: function (context) {
      $('input.ui-autocomplete-input', context).once('webform-autocomplete').each(function () {
        
        // If input value is an autocomplete match, reset the input to its default value.
        if (/\(([^)]+)\)$/.test(this.value)) {
          this.value = this.defaultValue;
        }

        $(this).bind('autocompleteselect', function (event, ui) {
          if (ui.item) {
            $(this).val(ui.item.value);
            this.form.submit();
          }
        });
      });
    }
  };

})(jQuery);